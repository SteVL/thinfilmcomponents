package thinfilmcomponents.controller;

/**
 *
 * @author сергей
 */
public class ResCalculationView {

    private String Alpha;
    private String DeltaAging;
    private String DeltaRsq;
    private String DeltaRk;
    private String P;
    private String P0;
    private String Prob;
    private String R;
    private String Rdop;
    private String Rsq;
    private String SigmaB;
    private String SigmaL;
    private String Tmax;
    private String method;

    public String getDeltaRk() {
        return DeltaRk;
    }

    public void setDeltaRk(String DeltaRk) {
        this.DeltaRk = DeltaRk;
    }

    public String getAlpha() {
        return Alpha;
    }

    public String getDeltaAging() {
        return DeltaAging;
    }

    public String getDeltaRsq() {
        return DeltaRsq;
    }

    public String getP() {
        return P;
    }

    public String getP0() {
        return P0;
    }

    public String getProb() {
        return Prob;
    }

    public String getR() {
        return R;
    }

    public String getRdop() {
        return Rdop;
    }

    public String getRsq() {
        return Rsq;
    }

    public String getSigmaB() {
        return SigmaB;
    }

    public String getSigmaL() {
        return SigmaL;
    }

    public String getTmax() {
        return Tmax;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setAlpha(String Alpha) {
        this.Alpha = Alpha;
    }

    public void setDeltaAging(String DeltaAging) {
        this.DeltaAging = DeltaAging;
    }

    public void setDeltaRsq(String DeltaRsq) {
        this.DeltaRsq = DeltaRsq;
    }

    public void setP(String P) {
        this.P = P;
    }

    public void setP0(String P0) {
        this.P0 = P0;
    }

    public void setProb(String Prob) {
        this.Prob = Prob;
    }

    public void setR(String R) {
        this.R = R;
    }

    public void setRdop(String Rdop) {
        this.Rdop = Rdop;
    }

    public void setRsq(String Rsq) {
        this.Rsq = Rsq;
    }

    public void setSigmaB(String SigmaB) {
        this.SigmaB = SigmaB;
    }

    public void setSigmaL(String SigmaL) {
        this.SigmaL = SigmaL;
    }

    public void setTmax(String Tmax) {
        this.Tmax = Tmax;
    }

}
