package thinfilmcomponents.controller;

import java.util.ArrayList;
import thinfilmcomponents.model.ResMaterialData;
import thinfilmcomponents.service.DBCon;

/**
 *
 * @author SteVL
 */
public class ResMaterialDataController {

    private DBCon con;

    public ResMaterialDataController() {
        //  DBCon=new DBCon("m")
    }

    public ArrayList<ResMaterialData> getAllMaterials() {
        ArrayList<ResMaterialData> arr = new ArrayList<ResMaterialData>();
        arr.add(new ResMaterialData("Хром ХО, ГОСТ 5905-67", 200, 500, 2.0, 1.8, 1.8, 0.6, 0.6, 2.0, 2.0));
        arr.add(new ResMaterialData("Тантал, СУО.021.041 ТУ", 300, 500, 2.0, -1.5, -1.5, -2.6, -2.6, 2.6, 3.2));        
        return arr;

        /*
                ArrayList<Task> arTasks = new ArrayList<>();
        String query = "SELECT id FROM task";
        Statement st = DBCon.con.createStatement();
        ResultSet rs = st.executeQuery(query);

        while (rs.next()) {
            Task task = new Task(rs.getInt("id"));
            arTasks.add(task);
        }
        return arTasks;
         */
    }
}
