package thinfilmcomponents.view;

import javax.swing.table.AbstractTableModel;
import thinfilmcomponents.model.ResMaterialData;

/**
 *
 * @author SteVL
 */
public class ResInfoTableModel extends AbstractTableModel {

    private ResMaterialData matData;

    public ResInfoTableModel(ResMaterialData matData) {
        this.matData = matData;
    }

    @Override
    public int getRowCount() {
        return 5;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "Параметр";
        }
        if (column == 1) {
            return "Значение";
        }
        return null;
    }

    /**
     *
     * @param rowIndex
     * @param columnIndex doesn't mean.
     * @return
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (rowIndex) {
            case 0:
                return matData.getRsqLeft()+"..."+ matData.getRsqRight();
            case 1:
                return matData.getP0();
            case 2:
                
        }
        return null;
    }

}
