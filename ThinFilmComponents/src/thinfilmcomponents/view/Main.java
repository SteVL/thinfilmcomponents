package thinfilmcomponents.view;

import java.sql.SQLException;
import org.apache.log4j.Logger;
import thinfilmcomponents.model.EngineerCalcData;
import thinfilmcomponents.model.MaterialCalcData;
import thinfilmcomponents.model.resistors.Calculator;
import thinfilmcomponents.model.resistors.ResistorExpert;

/**
 *
 *
 */
public class Main {

    private static final Logger log = Logger.getLogger(Main.class);

    private String resistorFormTitle;
    private String parameters;

    public void setResistorFormTitle(String resistorFormTitle) {
        this.resistorFormTitle = resistorFormTitle;
    }

    private void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public static void main(String[] args) throws SQLException {
//        log.trace("TRACE");//?
//        log.debug("DEBUG");
//        log.info("INFO");
//        log.warn("WARN");
//        log.error("ERROR");
//        log.fatal("FATAL");
        EngineerCalcData enCalcData = new EngineerCalcData.Builder()
                .setR(1000)
                .setDeltaRdop(10)
                .setP(10)
                .setProbPercent(50)
                .setTmax(125)
                .setDeltaRk(0)
                .setSigmaL(0.01)
                .setSigmaB(0.01)
                .setSigmaRsq(1)
                .setMethod(0)
                .build();
        double Rsq = 81;
        double P0 = 3;
        double alphaR = -2;
        double deltaAging = 5;
        MaterialCalcData matCalcData = new MaterialCalcData(Rsq, P0, alphaR, deltaAging);
        Calculator calculator = new Calculator(enCalcData, matCalcData);
        ResistorExpert resExpert = new ResistorExpert(calculator);
        //resExpert.consult();
       
       System.out.println(resExpert.getSolutionSequence());


    }
}
