package thinfilmcomponents.view;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import thinfilmcomponents.model.ResMaterialData;

/**
 *
 * @author SteVL
 */
public class ResMaterialTableModel extends AbstractTableModel {

    private List<ResMaterialData> arrMaterial;

    public ResMaterialTableModel(ArrayList<ResMaterialData> arrMaterial) {
        this.arrMaterial = arrMaterial;
    }

    @Override
    public int getRowCount() {
        return arrMaterial.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        if (column == 0) {
            return "Название";
        }
        if (column == 1) {
            return "Сопр.кв.плёнки, Ом";
        }
        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ResMaterialData matData = arrMaterial.get(rowIndex);
        if (columnIndex == 0) {
            return matData.getTitle();
        }
        if (columnIndex == 1) {
            return matData.getRsqLeft() + "..." + matData.getRsqRight();
        }
        return null;
    }

    public ResMaterialData getMaterial(int row) {
        return arrMaterial.get(row);
    }

}
