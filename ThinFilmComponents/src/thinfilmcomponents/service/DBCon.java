package thinfilmcomponents.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sqlite.SQLiteConfig;

/**
 * Класс описывает соединение с базой данных SQLite
 * @author Admin
 */
public class DBCon {

    private Connection con;

    public DBCon(String dbName) {
        initCon(dbName);
    }

    public Connection getCon() {
        return con;
    }

    private void initCon(String dbName) {
        try {
            Class.forName("org.sqlite.JDBC");
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            con = DriverManager.getConnection("jdbc:sqlite:" + dbName, config.toProperties());
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBCon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
