package thinfilmcomponents.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Заполняем таблицу БД для вероятностей из файла
 * @author SteVL
 */
public class ProbTable {

    public  void main(String[] args) throws IOException, SQLException {
        DBCon con = new DBCon("prob.db");
        Statement st = con.getCon().createStatement();

        File file = new File("probTable.txt");
        BufferedReader in = new BufferedReader(new FileReader(file));
        double z, F;
        String str;
        String[] arrStr;
        String query;
        System.out.println("Start Reading File and Write data to base");
        while ((str = in.readLine()) != null) {
            arrStr = str.split(" ");
            z = Double.valueOf(arrStr[0]);
            F = Double.valueOf(arrStr[1]);
            query = "INSERT INTO " + "probTable" + " (z, prob) "
                    + "VALUES (" + z + ", " + F + ")";

            //System.out.println(z + " " + F);
            //System.out.println(query);
            st.executeUpdate(query);
        }
        in.close();
        st.close();        
        System.out.println("done");
        System.out.println(con);
        con.getCon().close();

    }
}
