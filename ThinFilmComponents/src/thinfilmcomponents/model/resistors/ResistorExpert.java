package thinfilmcomponents.model.resistors;

import java.sql.SQLException;
import org.apache.log4j.Logger;
import thinfilmcomponents.model.EngineerCalcData;
import thinfilmcomponents.model.MaterialCalcData;

/**
 * Эксперт по резисторам. Исходя из расчитанных данных, определяет возможную
 * конструктивную форму ТПР и выдаёт параметры для проектирования.
 *
 * @author SteVL
 */
public class ResistorExpert {

    private static final Logger log = Logger.getLogger(ResistorExpert.class);

    private final Calculator calculator;

    //Данные, на основе которых принимается решение
    private double deltaR;
    private double deltaAging;
    private double deltaRt;
    private double znam;
    private double l;
    private double l_noReg;
    private double l_c;

    /**
     * Решение эксперта.
     */
    private String[] solution;

    /**
     * Ход рассуждений.
     */
    private StringBuilder solutionSequence;
    //переменные для решения   
    /**
     * Пригодность материала для изготовления резистора.
     */
    private boolean isMaterialApplicable;
    /**
     * Возможность изготовления резистора без подгонки.
     */
    private boolean isPossibProdNoAdj;
    /**
     * Возможность изготовления резистора с подгонкой.
     */
    private boolean isPossibProdAdj;

    public StringBuilder getSolutionSequence() {
        return solutionSequence;
    }

    public double getDeltaR() {
        return deltaR;
    }

    public String[] getSolution() {
        return solution;
    }

    public Calculator getCalculator() {
        return calculator;
    }

    /**
     * Вызывается (создаётся) эксперт для консультации.
     *
     */
    public ResistorExpert(Calculator calculator) {
        this.calculator = calculator;
        solutionSequence = new StringBuilder();
    }

    public void consult() throws SQLException, ToleranceException {
        String separator = "\n";

        calculator.calcDeltaR();
        deltaR = calculator.getDeltaR();
        if (deltaR > 0) {
            isMaterialApplicable = true;
            solutionSequence.append("Материал пригоден для изготовления резистора без подгонки, т.к. deltaR > 0.").append(separator);
        } else {
            isMaterialApplicable = false;
            solutionSequence.append("Материал не пригоден для изготовления резистора без подгонки, т.к. deltaR <= 0.").append(separator);
            throw new ToleranceException("Материал не пригоден для изготовления резистора без подгонки, т.к. deltaR <= 0.");
            //будет возможность посмотреть ход решения. Почему deltaR<=0
            //return;
        }
        calculator.calcZnam();
        znam = calculator.getZnam();
        if (znam > 0) {
            isPossibProdNoAdj = true;
            solutionSequence.append("Можно изготовить резистор без подгонки, т.к. znam > 0").append(separator);
        } else {
            isPossibProdNoAdj = false;
            solutionSequence.append("Нельзя изготовить резистор без подгонки, т.к. znam <= 0").append(separator);
        }

        if (isPossibProdNoAdj) {
            calculator.calcParamsForSquareForm();
            l = calculator.get_l();
            if (l <= 8) {
                solutionSequence.append("Можно изготовить прямоугольный резистор без подгонки, т.к. l <= 8.").append(separator);
                buildSolution(0);
            } else {
                solutionSequence.append("Нельзя изготовить прямоугольный резистор без подгонки, т.к. l>8. Нужно изготавливать в форме меандра без подгонки.").append(separator);
                calculator.calcParamsForMeanderForm();
                buildSolution(1);
            }
        }

        if (!isPossibProdNoAdj) {
            if (calculator.getSumDeltaRtDeltaAging() <= deltaR) {
                isPossibProdAdj = true;
                solutionSequence.append("Можно изготовить резистор с подгонкой, т.к. deltaRt+deltaRst <= deltaR.").append(separator);
            } else {
                isPossibProdAdj = false;
                solutionSequence.append("Материал не пригоден для изготовления резистора c подгонкой, т.к. deltaRt+deltaRst > deltaR.").append(separator);
//                return;
            }
        }

        if (isPossibProdAdj) {
            calculator.calcTagParamsOfAdj();
            l_noReg = calculator.getL_noReg();
            l_c = calculator.getL_c();
            if (l_noReg < 8 && l_c >= 0.2) {
                calculator.calcParamsForSquareAdjStage();
                buildSolution(2);
            } else if (l_noReg < 8 && l_c < 0.2) {
               calculator.calcParamsForSquareAdjSmooth();
                buildSolution(3);
            } else if (l_noReg >= 8 && l_c >= 0.2) {
                System.out.println("Меандр со ступ. подгонкой.");
                throw new UnsupportedOperationException("Расчёт меандра со ступ. подгонкой не реализован.");
            } else if (l_noReg >= 8 && l_c < 0.2) {
                System.out.println("Меандр с плавной подгонкой.");
                throw new UnsupportedOperationException("Расчёт меандра с плавной подгонкой вообще возможен?");
            }
        }
    }

    private void buildSolution(int flag) {
        if (log.isDebugEnabled()) {
            log.debug("Enter to method");
        }
        String type = null;
        StringBuilder params = new StringBuilder();

        switch (flag) {
            case 0:
                type = "Прямоугольный без подгонки";
                params.append("Длина ТПР, мм:\n")
                        .append("L = ").append(calculator.getL()).append("\n")
                        .append("Ширина ТПР, мм:\n")
                        .append("B = ").append(calculator.getB());
                break;
            case 1:
                type = "Меандр без подгонки";
                params.append("Длина ТПР, мм:\n")
                        .append("L = ").append(calculator.getL()).append("\n")
                        .append("Ширина ТПР, мм:\n")
                        .append("B = ").append(calculator.getB()).append("\n")
                        .append("Оптим. число перегибов:\n")
                        .append("Nopt = ").append(calculator.getNopt()).append("\n");
                break;
            case 2:
                type = "Прямоугольный со ступенчатой подгонкой";
                params.append("Длина ТПР, мм:\n")
                        .append("L = ").append(calculator.getL()).append("\n")
                        .append("Ширина ТПР, мм:\n")
                        .append("B = ").append(calculator.getB()).append("\n")
                        .append("Длина нерег.части, мм:\n")
                        .append("lн.р. = ").append(calculator.getL_noReg()).append("\n")
                        .append("Длина рег.части, мм:\n")
                        .append("lc = ").append(calculator.getL_c()).append("\n")
                        .append("Число секций:\n")
                        .append("n = " + calculator.getN()).append("\n")
                        .append("Длина перемычек, мм:\n")
                        .append("ln = ").append(calculator.getL_N());
                break;
            case 3:
                type = "Прямоугольный с плавной подгонкой";
                params.append("Длина ТПР, мм:\n")
                        .append("L = ").append(calculator.getL()).append("\n")
                        .append("Ширина ТПР, мм:\n")
                        .append("B = ").append(calculator.getB()).append("\n")
                        .append("Длина нерег.части, мм:\n")
                        .append("lн.р. = ").append(calculator.getL_noReg()).append("\n")
                        .append("Ширина ТПР на подстраив. уч, мм:\n")
                        .append("b* = ").append(calculator.getbStar()).append("\n");
                break;
        }
        solution = new String[2];
        solution[0] = type;
        solution[1] = params.toString();
    }
}
