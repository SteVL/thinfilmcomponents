package thinfilmcomponents.model.resistors;

/**
 *
 * @author SteVL
 */
public class ToleranceException extends Exception {

    public ToleranceException(String message) {
        super(message);
    }
}
