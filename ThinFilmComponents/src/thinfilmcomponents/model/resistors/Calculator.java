package thinfilmcomponents.model.resistors;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.Logger;
import thinfilmcomponents.model.EngineerCalcData;
import thinfilmcomponents.model.MaterialCalcData;
import thinfilmcomponents.service.DBCon;

/**
 * Рассчитываемые данные, необходимые для определения формы резистора, а также
 * параметры формы.
 *
 *
 * @author SteVL
 */
public class Calculator {

    private static final Logger log = Logger.getLogger(Calculator.class);

    private final EngineerCalcData enCalcData;
    private final MaterialCalcData matCalcData;

    /*Данные, необходимые для выдачи решения */
    private double deltaR;
    private double deltaAging;
    private double deltaRt;
    private double znam;
    private double l;
    private double l_noReg;
    private double l_c;

    /* Данные, необходимые для расчёта данных, которые требуются для выдачи решения.*/
    private double K;
    private double b1, b2, b3, b;
    private double l1, l2, l3;
    private double sigmaRdop;
    private double z;

    /*Длина и ширина ТПР*/
    private double L;
    private double B;

    /*Параметры для меандра*/
    private double hm;
    private int Nopt;

    private double l_sq;

    /*Параметры для подгоночных резисторов*/
    private double R_noReg;
    private double R_reg;
    private int n;

    /**
     * Длина перемычек при ступенчатой подгонке.
     */
    private final double L_N = 0.2;

    /*Параметры для плавной подгонки*/
    private double l_min;
    private double bStar;

    /**
     * Создаёт объект для расчёта необходимых параметров.
     *
     * @param enCalcData данные от проектировщика
     * @param matCalcData данные материала, выбранного проектировщиком из
     * таблицы материалов
     */
    public Calculator(EngineerCalcData enCalcData, MaterialCalcData matCalcData) {
        this.enCalcData = enCalcData;
        this.matCalcData = matCalcData;
        //Нужна проверка на верный диапазон значений.
    }

    //<editor-fold defaultstate="collapsed" desc="Getters">
    public double getDeltaRt() {
        return deltaRt;
    }

    public double getDeltaR() {
        return deltaR;
    }

    public double getK() {
        return K;
    }

    public double getB1() {
        return b1;
    }

    public double getB2() {
        return b2;
    }

    public double getB3() {
        return b3;
    }

    public double getL1() {
        return l1;
    }

    public double getL2() {
        return l2;
    }

    public double getL3() {
        return l3;
    }

    public double getZnam() {
        return znam;
    }

    public double getSigmaRdop() {
        return sigmaRdop;
    }

    public double getZ() {
        return z;
    }

    public double get_l() {
        return l;
    }

    public double getL() {
        return L;
    }

    public double getB() {
        return B;
    }

    public double getMaskThickness() {
        return hm;
    }

    public int getNopt() {
        return Nopt;
    }

    /**
     * Длина прямоугольных участков.
     *
     * @return
     */
    public double getL_sq() {
        return l_sq;
    }

    public double getSumDeltaRtDeltaAging() {
        return deltaRt + deltaAging;
    }

    /**
     * Сопротивление нерегулируемой части.
     *
     * @return
     */
    public double getR_noReg() {
        return R_noReg;
    }

    public double getL_noReg() {
        return l_noReg;
    }

    public double getL_c() {
        return l_c;
    }

    public int getN() {
        return n;
    }

    public double getL_N() {
        return L_N;
    }

    //Геттеры для плавной подгонки
    public double getL_min() {
        return l_min;
    }

    public double getbStar() {
        return bStar;
    }
    //</editor-fold>

    /**
     * Рассчитывает коэффициент формы.
     *
     */
    private void calcK() {
        K = enCalcData.getR() / matCalcData.getRsq();
        if (log.isDebugEnabled()) {
            log.debug("K = " + K);
        }
    }

    //***********Посмотреть таблицу
    /**
     * Определяет длину l' (ширину b'), исходя из выбранного метода
     * изготовления.
     *
     * @return для метода фотолитографии значение 0.1 мм, для метода свободной
     * маски значение 0.2 мм.
     */
    private double defineParamMethod() {
        int method = enCalcData.getMethod();
        double param;
        if (method == 0) {
            param = 0.1;
            if (log.isDebugEnabled()) {
                log.debug("param set (photolithography) = " + param);
            }
            return param;
        } else if (method == 1) {
            param = 0.2;
            if (log.isDebugEnabled()) {
                log.debug("param set (mask method) = " + param);
            }
            return param;
        }
        return -1;
    }

    /**
     * Рассчитывает ширину b' '.
     *
     * @return
     */
    private void calcB2() {
        double P = enCalcData.getP();
        double P0 = matCalcData.getPzero();
        b2 = Math.sqrt(P / (P0 * K));
        if (log.isDebugEnabled()) {
            log.debug("b'' = " + b2);
        }
    }

    /**
     * Расчитывает ширину b' ' '.
     *
     * @return
     * @throws SQLException
     */
    private void calcB3() throws SQLException {
        double sigmaL = enCalcData.getSigmaL();
        double sigmaB = enCalcData.getSigmaB();
        double chisl = Math.pow(sigmaL / getK(), 2) + Math.pow(sigmaB, 2);
        if (log.isDebugEnabled()) {
            log.debug("chisl of b''' = " + chisl);
        }
        b3 = Math.sqrt(chisl / znam);
        if (log.isDebugEnabled()) {
            log.debug("b''' = " + b3);
        }
    }

    /**
     * Рассчитывает длину l' '.
     *
     * @return
     */
    private void calcL2() {
        double P = enCalcData.getP();
        double P0 = matCalcData.getPzero();
        l2 = Math.sqrt(P / P0) * getK();
        if (log.isDebugEnabled()) {
            log.debug("l' ' = " + l2);
        }
    }

    /**
     * Рассчитывает длину l' ' '.
     *
     * @return
     */
    private void calcL3() throws SQLException {
        double sigmaL = enCalcData.getSigmaL();
        double sigmaB = enCalcData.getSigmaB();
        double chisl = Math.pow(sigmaL, 2) + Math.pow((sigmaB * K), 2);
        if (log.isDebugEnabled()) {
            log.debug("chisl of l''' = " + chisl);
        }
        l3 = Math.sqrt(chisl / znam);
        if (log.isDebugEnabled()) {
            log.debug("l' ' ' = " + l3);
        }
    }

    /**
     * Рассчитывает sigmaRdop
     *
     * @throws SQLException
     */
    private void calcSigmaRdop() throws SQLException {
        double deltaR = getDeltaR();
        z = defineZ(enCalcData.getProbPercent());
        sigmaRdop = deltaR / (z * Math.sqrt(2));
        if (log.isDebugEnabled()) {
            log.debug("sigmaRdop = " + sigmaRdop);
        }
    }

    /**
     * Рассчитывает знаменатель b' ' ' (l' ' ').
     *
     * @throws SQLException
     */
    void calcZnam() throws SQLException {
        double sigmaRsq = enCalcData.getSigmaRsq() / 100;
        calcSigmaRdop();
        znam = Math.pow(sigmaRdop, 2) - Math.pow(sigmaRsq, 2);
        if (log.isDebugEnabled()) {
            log.debug("znam of b''' (l''') = " + znam);
        }
    }

    /**
     * Получает максимальное значение.
     *
     * @param arr
     * @return
     */
    private double getMax(double[] arr) {
        double max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }

    /**
     * Рассчитывает относительную погрешность, обусловленную воздействием
     * температуры окружающей среды в процессе эксплуатации.
     */
    private void calcDeltaRt() {
        double alphaR = matCalcData.getAlphaR() * Math.pow(10, -4);
        if (log.isDebugEnabled()) {
            log.debug("alphaR*10^-4 = " + alphaR);
        }
        double tmax = enCalcData.getTmax();
        deltaRt = alphaR * (tmax - 20) / 100;
        if (log.isDebugEnabled()) {
            log.debug("deltaRt/100 = " + deltaRt);
        }
    }

    /**
     * Расчитывает технологический допуск.
     *
     */
    void calcDeltaR() {
        //в дробях (в контроллер). А может и не надо.
        double deltaRdop = enCalcData.getDeltaRdop() / 100;
        if (log.isDebugEnabled()) {
            log.debug("deltaRdop/100 = " + deltaRdop);
        }
        deltaAging = matCalcData.getDeltaAging() / 100;
        if (log.isDebugEnabled()) {
            log.debug("deltaAging/100 = " + deltaAging);
        }
        double deltaRk = enCalcData.getDeltaRk() / 100;
        if (log.isDebugEnabled()) {
            log.debug("deltaRk/100 = " + deltaRk);
        }
        calcDeltaRt();
        deltaR = deltaRdop - deltaRt - deltaAging - deltaRk;
        if (log.isDebugEnabled()) {
            log.debug("deltaR = " + deltaR);
        }
    }

    /**
     * Поиск значения аргумента интеграла вероятностей Ф. Таблица вероятностей
     * хранится в БД(prob.db). Подбор осуществляется путём сравнения отклонения
     * (разности по модулю) заданного значения вероятности и последующего и
     * предыдущего табличного значения вероятности.
     *
     * @param probPercent процент(%) вероятности годных.
     * @return значение аргумента z.
     * @throws java.sql.SQLException
     */
    private double defineZ(double probPercent) throws SQLException {
        //*********проверка на диапазон значений (0;100)
        double prob = probPercent / 100;
        if (log.isDebugEnabled()) {
            log.debug("prob=probPercent/100 = " + prob);
        }
        DBCon con = new DBCon("prob.db");

        Statement st = con.getCon().createStatement();
        String query = "SELECT z, prob FROM " + "probtable";

        ResultSet rs = st.executeQuery(query);
        //предусмотреть возможность более точной таблицы вероятностей
        double[] z = new double[113];
        double[] probT = new double[113];
        int count = 0;
        while (rs.next()) {
            z[count] = rs.getDouble("z");
            probT[count] = rs.getDouble("prob");
            count++;
        }
        rs.close();
        st.close();

        int indexPrev = 0, indexNext = 0;

        //ищем приблеженное значение Ф
        for (int cnt = 0; cnt < probT.length; cnt++) {
            if (probT[cnt] >= prob) {
                //запоминаем индекс, для сравнения с предыдущим значением
                indexNext = cnt;
                break;
            }
            //если табличное значение меньше искомого, то ищем дальше, но
            //запоминаем индекс;
            indexPrev = cnt;
        }
        //ищем значение Фтабл с меньшим отклонением от Ф
        //имеем два значения: меньше Ф и больше Ф
        double a = Math.abs(prob - probT[indexPrev]);
        double b = Math.abs(prob - probT[indexNext]);

        double resZ;
        if (a < b) {
            resZ = z[indexPrev];
        } else {
            resZ = z[indexNext];
        }
        if (log.isDebugEnabled()) {
            log.debug("z = " + resZ);
        }
        return resZ;
    }

    /**
     * Рассчитывает значение l и размеры прямоугольного ТПР без подгонки.
     *
     * @throws SQLException
     */
    void calcParamsForSquareForm() throws SQLException {
        if (log.isDebugEnabled()) {
            log.debug("Сalculating Square form..");
        }
        calcK();
        if (K >= enCalcData.getSigmaL() / enCalcData.getSigmaB()) {
            b1 = defineParamMethod();
            calcB2();
            calcB3();
            double[] arr = {b1, b2, b3};
            b = getMax(arr);
            l = b * K;
        } else {
            l1 = defineParamMethod();
            calcL2();
            calcL3();
            double[] arr = {l1, l2, l3};
            l = getMax(arr);
            b = l / K;
        }

        if (log.isDebugEnabled()) {
            log.debug(" l =" + l);
        }
        double h = 0.2;
        L = l + 2 * h;
        B = b;
        if (log.isDebugEnabled()) {
            log.debug("L=" + L);
            log.debug("B=" + B);
        }
    }

    /**
     * Определяет значение толщины маски в зависимости от выборанного метода
     * изготовления.
     *
     * @return для метода фотолитографии значение 0.1 мм, для метода свободной
     * маски значение 0.02 мм.
     */
    private double defineMaskThickness() {
        int method = enCalcData.getMethod();
        double param;
        if (method == 0) {
            param = 0.02;
            if (log.isDebugEnabled()) {
                log.debug("maskThickness set (photolithography) = " + param);
            }
            return param;
        } else if (method == 1) {
            param = 0.1;
            if (log.isDebugEnabled()) {
                log.debug("maskThickness set (mask method) = " + param);
            }
            return param;
        }
        return -1;
    }

    /**
     * Рассчитывает параметры меандра без подгонки.
     */
    void calcParamsForMeanderForm() {
        hm = defineMaskThickness();
        double a = 2 * hm;
        if (log.isDebugEnabled()) {
            log.debug("a = " + a);
        }
        Nopt = (int) Math.sqrt(K / 2);
        if (log.isDebugEnabled()) {
            log.debug("Nopt = " + Nopt);
        }
        double Rsq = matCalcData.getRsq();
        double R1 = 2.55 * Rsq;
        double R2 = 4 * matCalcData.getRsq();
        l_sq = (enCalcData.getR() - (R1 + R2)) * b / (Rsq * Nopt);
        if (log.isDebugEnabled()) {
            log.debug("length square areas =" + l_sq);
        }
        L = Nopt * a + (Nopt + 1) * b;
        if (log.isDebugEnabled()) {
            log.debug("L = " + L);
        }
        B = l_sq + 4 * b;
        if (log.isDebugEnabled()) {
            log.debug("B = " + B);
        }
    }

    private void calcR_noReg() {
        R_noReg = enCalcData.getR() / (1 + 3 * sigmaRdop);
        if (log.isDebugEnabled()) {
            log.debug("R_noReg = " + R_noReg);
        }
    }

    private void calcR_reg() {
        R_reg = enCalcData.getR() * (6 * sigmaRdop / (1 - 9 * Math.pow(sigmaRdop, 2)));
        if (log.isDebugEnabled()) {
            log.debug("R_reg = " + R_reg);
        }
    }

    private void calc_lnoReg() {
        l_noReg = (R_noReg * b) / matCalcData.getRsq();
        if (log.isDebugEnabled()) {
            log.debug("l_noReg = " + l_noReg);
        }
    }

    private void calc_lc() {
        l_c = (R_reg * b) / (matCalcData.getRsq() * n);
        if (log.isDebugEnabled()) {
            log.debug("l_c = " + l_c);
        }
        if (l_c < 0.2) {
            l_c = l_c = (2 * R_reg * b) / (matCalcData.getRsq() * n);
            if (log.isDebugEnabled()) {
                log.debug("l_c = " + l_c);
            }
        }

    }

    /**
     * Рассчитывает длины нерегулируемой и регулируемой частей.
     */
    void calcTagParamsOfAdj() throws SQLException {
        calcR_noReg();
        calcR_reg();
        n = (int) ((6 * sigmaRdop) / (deltaR * (1 - 3 * sigmaRdop)));
        if (log.isDebugEnabled()) {
            log.debug("num sections = " + n);
        }
        calcK();
        b1 = defineParamMethod();
        calcB2();
        double[] arr = {b1, b2};
        b = getMax(arr);
        if (log.isDebugEnabled()) {
            log.debug("b = " + b);
        }
        calc_lnoReg();
        calc_lc();
    }

    /**
     * Рассчитывает параметры прямоугольного со ступенчатой подгонкой.
     */
    void calcParamsForSquareAdjStage() {
        double h = 0.2;
        L = l_noReg + 2 * h + n * (L_N + l_c);
        B = b;
    }

    private void calc_l_min() {
        l_min = (enCalcData.getR() * (1 - deltaR) * (b + 3 * enCalcData.getSigmaB()))
                / (matCalcData.getRsq() * (1 - 3 * enCalcData.getSigmaRsq()));
    }

    void calcParamsForSquareAdjSmooth() {
        calc_l_min();
        l = l_min + 3 * enCalcData.getSigmaL();
        bStar = ((l * matCalcData.getRsq() * (1 + 3 * enCalcData.getSigmaRsq()))
                / enCalcData.getR() * (1 + enCalcData.getSigmaRsq())) + 3 * enCalcData.getSigmaB();
        double h = 0.2;
        L = l + 2 * h;
        B = b;

    }

}
