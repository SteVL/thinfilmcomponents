package thinfilmcomponents.model;

/**
 * Данные материала считанного из БД по запросу относительно Rкв.
 *
 * @author SteVL
 */
public class ResMaterialData {

    private String title;
    private double RsqLeft, RsqRight;
    private double P0;
    private double alphaMinL, alphaMaxL;
    private double alphaMinR, alphaMaxR;
    private double deltaStMin, deltaStMax;

    public ResMaterialData() {
    }

    public ResMaterialData(String title, double RsqLeft, double RsqRight, double P0, double alphaMinL, double alphaMaxL, double alphaMinR, double alphaMaxR, double deltaStMin, double deltaStMax) {
        this.title = title;
        this.RsqLeft = RsqLeft;
        this.RsqRight = RsqRight;
        this.P0 = P0;
        this.alphaMinL = alphaMinL;
        this.alphaMaxL = alphaMaxL;
        this.alphaMinR = alphaMinR;
        this.alphaMaxR = alphaMaxR;
        this.deltaStMin = deltaStMin;
        this.deltaStMax = deltaStMax;
    }

    public String getTitle() {
        return title;
    }

    public double getRsqLeft() {
        return RsqLeft;
    }

    public double getRsqRight() {
        return RsqRight;
    }

    public double getP0() {
        return P0;
    }

    public double getAlphaMinL() {
        return alphaMinL;
    }

    public double getAlphaMaxL() {
        return alphaMaxL;
    }

    public double getAlphaMinR() {
        return alphaMinR;
    }

    public double getAlphaMaxR() {
        return alphaMaxR;
    }

    public double getDeltaStMin() {
        return deltaStMin;
    }

    public double getDeltaStMax() {
        return deltaStMax;
    }

    private void create() {

    }

    private void read() {

    }

    private void update() {

    }

    private void delete() {

    }

}
