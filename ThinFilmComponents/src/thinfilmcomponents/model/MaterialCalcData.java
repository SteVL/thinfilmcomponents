package thinfilmcomponents.model;

/**
 * Данные материала, выбранные проектировщиком для расчёта.
 *
 * @author SteVL
 */
public class MaterialCalcData {

    //сопр. квадрата плёнки
    private double Rsq;
    //уд.мощность
    private double Pzero;
    //отн. изменение сопр. плёнки в результате старения
    private double deltaAging;
    //темп.коэф сопртивления
    private double alphaR;//10^-4;

    /**
     * Создаёт данные материала необходимых для расчёта.
     *
     * @param Rsq сопр. квадрата плёнки
     * @param Pzero уд. мощность
     * @param alphaR темп.коэф сопртивления
     * @param deltaAging отн. изменение сопр. плёнки в результате старения
     */
    public MaterialCalcData(double Rsq, double Pzero, double alphaR, double deltaAging) {
        this.Rsq = Rsq;
        this.Pzero = Pzero;
        this.alphaR = alphaR;
        this.deltaAging = deltaAging;
    }
    //А нужны лы сеттеры?

    public double getRsq() {
        return Rsq;
    }

    public double getPzero() {
        return Pzero;
    }

    public double getDeltaAging() {
        return deltaAging;
    }

    public double getAlphaR() {
        return alphaR;
    }
}
