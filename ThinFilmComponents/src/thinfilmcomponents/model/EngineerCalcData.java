package thinfilmcomponents.model;

/**
 * Расчётные данные, полученные от проектировщика.
 *
 * @author SteVL
 */
public class EngineerCalcData {

    private double R;//Сопротивление
    private double deltaRdop;//допуск на сопротивление
    private double P;//Рассеиваемая мощность
    private double ProbPercent;//вероятность годных (%)
    private double tmax;//макс.раб.температура
    private double deltaRk;//погрешность, обусл. образованием контактов
    private double sigmaL;//скво длины
    private double sigmaB;//скво ширины
    private double sigmaRsq;//отн. скво Rкв в процессе изготовления
    private int method; //метод изготовления (0 - фотолитография, 1 - метод маски)

//    public EngineerCalcData(double R, double deltaR, double P, double ProbPercent, double tmax, double deltaRk, double sigmaL, double sigmaB, double sigmaRsq) {
//        this.R = R;
//        this.deltaR = deltaR;
//        this.P = P;
//        this.ProbPercent = ProbPercent;
//        this.tmax = tmax;
//        this.deltaRk = deltaRk;
//        this.sigmaL = sigmaL;
//        this.sigmaB = sigmaB;
//        this.sigmaRsq = sigmaRsq;
//    }

    public static class Builder {

        private double R;
        private double deltaRdop;
        private double P;
        private double ProbPercent;
        private double tmax;
        private double deltaRk;
        private double sigmaL;
        private double sigmaB;
        private double sigmaRsq;
        private int method;
        
        public Builder setR(double R) {
            this.R = R;
            return this;
        }

        public Builder setDeltaRdop(double deltaRdop) {
            this.deltaRdop = deltaRdop;
            return this;
        }

        public Builder setP(double P) {
            this.P = P;
            return this;
        }

        public Builder setProbPercent(double ProbPercent) {
            this.ProbPercent = ProbPercent;
            return this;
        }

        public Builder setTmax(double tmax) {
            this.tmax = tmax;
            return this;
        }

        public Builder setDeltaRk(double deltaRk) {
            this.deltaRk = deltaRk;
            return this;
        }

        public Builder setSigmaL(double sigmaL) {
            this.sigmaL = sigmaL;
            return this;
        }

        public Builder setSigmaB(double sigmaB) {
            this.sigmaB = sigmaB;
            return this;
        }

        public Builder setSigmaRsq(double sigmaRsq) {
            this.sigmaRsq = sigmaRsq;
            return this;
        }
        
        /**
         * Метод изготовления.
         * @param method (0 - фотолитография, 1 - метод маски).
         * @return
         */
        public Builder setMethod(int method){
            this.method=method;
            return this;
        }

        public EngineerCalcData build() {
            return new EngineerCalcData(this);
        }

    }

    private EngineerCalcData(Builder builder) {
        R = builder.R;
        deltaRdop = builder.deltaRdop;
        P = builder.P;
        ProbPercent = builder.ProbPercent;
        tmax = builder.tmax;
        deltaRk = builder.deltaRk;
        sigmaL = builder.sigmaL;
        sigmaB = builder.sigmaB;
        sigmaRsq = builder.sigmaRsq;
        method=builder.method;
    }

    public double getR() {
        return R;
    }

    public double getDeltaRdop() {
        return deltaRdop;
    }

    public double getP() {
        return P;
    }

    public double getProbPercent() {
        return ProbPercent;
    }

    public double getTmax() {
        return tmax;
    }

    public double getDeltaRk() {
        return deltaRk;
    }

    public double getSigmaL() {
        return sigmaL;
    }

    public double getSigmaB() {
        return sigmaB;
    }

    public double getSigmaRsq() {
        return sigmaRsq;
    }

    public int getMethod() {
        return method;
    }
}
